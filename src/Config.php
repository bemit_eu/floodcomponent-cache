<?php

namespace Flood\Component\Cache;

/**
 * Config connector for this component.
 *
 * @category
 * @package    Flood\Component\Cache
 * @author     Original Author michael@bemit.codes
 * @link
 * @copyright  2017 Michael Becker
 * @since      Version 0.2
 * @version    Release: @package_version@
 */

class Config {

    protected static $data;

    public static function addData($data) {
        static::$data = $data;
    }

    protected static function getConfig($var_key = null) {
        return static::$data[$var_key];
    }

    /**
     * Returns the absolute path to project root, not this component root
     *
     * @return string
     */
    public static function serverPath() {
        return static::getConfig('server-path');
    }

    /**
     * Returns the relative path to cache folder, must be concated
     *
     * @return string
     */
    public static function storagePath() {
        return static::serverPath() . '/' . static::getConfig('storage-path');
    }

    /**
     * If cache is active
     *
     * @return bool
     */
    public static function active() {
        return static::getConfig('active');
    }

    /**
     * @return bool
     */
    public static function checkUpToDate() {
        return static::getConfig('check-up-to-date');
    }
}