<?php

namespace Flood\Component\Cache;

/**
 * Caching class, could be instantiated before Config is read, but only when using readSame
 *
 * @package    Flood\Component\Cache
 * @author     Original Author, Michael Becker, michael@bemit.codes
 * @link
 * @copyright  2017 Michael Becker
 * @since
 * @version    Release: @package_version@
 */

class Cache {
    /**
     * @var self $i
     */
    protected static $i = null;

    public static $debug = false;

    public function __construct() {
    }

    /**
     * @return \Flood\Component\Cache\Cache
     */
    public static function i() {
        if (null === static::$i) {
            if (get_class() !== get_called_class()) {
                $tmp = get_called_class();
                static::$i = new $tmp;
            } else {
                static::$i = new self;
            }
        }

        return static::$i;
    }

    /**
     * Reads and generates a cached JSON file
     *
     * @param string $origin relative path to the file which should be cached
     *
     * @return bool|mixed false when not able to read file (or the only value) of the cached file
     */
    public function read($origin) {
        $return_val = false;
        if (is_file(Config::serverPath() . '/' . $origin)) {
            // Only when origin file exists, make anything
            $cache_path = $this->makeCachePath($origin); // Generate the path to the storage
            if (is_file($cache_path) && Config::active() && $this->checkUpToDate($origin, $cache_path)
            ) {
                // When the file is already cached and cache is active and cache is up-to-date
                $return_val = include $cache_path;// Get the array data directly from cached file
            } else {
                // When the file is not cached
                $return_val = $this->generateCachedFile(Config::serverPath() . '/' . $origin, $cache_path, Config::active());
            }
        }

        return $return_val;
    }

    /**
     * Writes data to a cached JSON file
     *
     * @param array  $data       data that should be cached
     * @param string $cache_name name of the cached element, should be as unique as possible to not conflict with automatic generated cache files
     * @param bool   $overwrite  when true the data will be overwritten, when false the data will me recursivly merged
     *
     * @return bool|mixed false when not able to write file (or the only value) of the cached file
     */
    public function write($data, $cache_name, $overwrite = true) {
        $return_val = false;
        if (is_array($data)) {
            $origin = Config::serverPath() . '/';
            // Only when origin file exists, make anything
            $cache_path = $this->makeCachePath($origin); // Generate the path to the storage
            if (is_file($cache_path) && Config::active() && $this->checkUpToDate($origin, $cache_path)
            ) {
                // When the file is already cached and cache is active and cache is up-to-date
                $return_val = include $cache_path;// Get the array data directly from cached file
            } else {
                // When the file is not cached
                $return_val = $this->generateCachedFile(Config::serverPath() . '/' . $origin, $cache_path, Config::active());
            }
        }

        return $return_val;
    }

    /**
     * Reads and generates a cached JSON file directly in the same folder where the origin is
     * This could for example be used to cache the config json files
     *
     * @param string $origin absolute path to cached file
     *
     * @return bool|mixed false when not able to read file (or the only value) or the content of the cached file
     */
    public function readSame($origin) {
        $return_val = false;
        if (is_file($origin)) {
            // Only when origin file exists, make anything
            $cache_path = $origin . '.php';

            if (is_file($cache_path) && (filemtime($origin) < filemtime($cache_path)) && false) {
                // When the file is already cached and is up-to-date
                $return_val = include $cache_path;// Get the array data directly from cached file
            } else {
                // When the file is not cached
                $return_val = $this->generateCachedFile($origin, $cache_path);
            }
        } else {
            error_log('FloodCache: is not a file: ' . $origin);
        }

        return $return_val;
    }


    /**
     * Checks if the given origin is newer than the given cache_path, if Config::checkUpToDate() is false every item will be up-to-date
     *
     * @param string $origin     relative path to the too cached file
     * @param string $cache_path absolute path to the already cached file
     *
     * @return bool true when file already up-to-date, false when not
     */
    protected function checkUpToDate($origin, $cache_path) {
        $return_val = true;
        if (Config::checkUpToDate()) {
            if (filemtime(Config::serverPath() . '/' . $origin) > filemtime($cache_path)) {
                $return_val = false;
            }
        }

        return $return_val;
    }

    public function getLastUpdate($origin) {
        return filemtime(Config::serverPath() . '/' . $origin);
    }

    /**
     * Generates the complete path for the cached element, hashed origin dir with sha1
     *
     * @param $origin
     *
     * @return string
     */
    protected function makeCachePath($origin) {
        $origin_filename = $this->makeOriginFilename($origin);

        return Config::storagePath() . hash('sha1', dirname($origin)) . $origin_filename . '.php'; // Generate the path to the storage
    }

    /**
     * Makes only the filename out of the complete origin path
     *
     * @param $origin
     *
     * @return string
     */
    protected function makeOriginFilename($origin) {
        $tmp = strrev(substr(strrev($origin), 0, strpos(strrev($origin), '/'))); // Cut everything after last / in $file

        return substr($tmp, 0, strpos($tmp, '.')); // Cut everything after first . in $tmp
    }

    /**
     * Writes the cached file to the folder and returns the read content
     *
     * @param string $origin     absolute path of origin file
     * @param string $cache_path absolute path where the file should be cached
     * @param bool   $write      if the read content should really be written or only the not cached file should be read
     *
     * @return bool|mixed
     */
    protected function generateCachedFile($origin, $cache_path, $write = true) {
        $return_val = \Flood\Component\Json\Json::decodeFile($origin, true);// Read the JSON from the uncached file

        //var_dump($return_val);
        if ($write && !empty($return_val)) {
            file_put_contents($cache_path, '<?php' . "\n" . 'return ' . var_export($return_val, true) . ';');// Generate and write the data into the cached file
        }

        return $return_val;
    }
}
